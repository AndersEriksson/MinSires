# MinSires v. 1.0

Implementation of the MinSires algorithm.<br>
Reference: Eriksson et al. (2010) Molecular Ecology Resources 10(2):282-291

Anders Eriksson
Department of Medical and Molecular Genetics
Kings College London
Great Maze Pond
London SE1 9RT 
Email: anders.eriksson@kcl.ac.uk

## Installation

To compile from source, use a standard C++ compiler, e.g.

g++ -O3 -o minsires minsires.cpp

## Running

The program takes allele data in standard in, and outputs the result ot standard out. A typical invocation of the program is

minsires < Family_data_Makinen_etal.txt > example_output.txt

where input is the text file 'Family_data_Makinen_etal.txt', containing microsatellite genetic data from Makinen et al (2007, J. Heredity, 98, 705Ð711), and the output is saved in the file 'example_output.txt'.

The formating of the input file is as follows: Each family is represented by one line for the mother and one line for each of the progeny. Between families must come one or more empty lines. Each line in the family begins with a text string (with no spaces) containing the identifier for the individual. The dame's identifier must begin with the letter m, but the progenies' labels may contain any letters or digits. After the identifier comes the genetic information of the loci. The alleles of each locus is represent as two integers separated by spaces. Missing data is represented by -1.

In running the program as above, an warning is produced:

*** Warning: Ignoring apparent null allele in child 12 locus 2 = [213 213], dame = [216 222]

This warning shows that an apparent null allele has been found because none of the alleles for locus two stems from the dame. The program assumes that the contribution from the sire is allele 213, and continues. Alternatively, one may edit the input file to replace these with minus ones, so that the locus will be treated as missing data in that individual.

## Options

The program accepts two options:

The option 'useFamily' makes the program analyse only a given family. This is useful if running the analysis in parallell from an input file containing several families. For example,

./minsires useFamily=2 < Family_data_Makinen_etal.txt

restricts the analysis to the second family in 'Family_data_Makinen_etal.txt'.

The option 'quiet', when set to true (quiet=t) causes the program to print only the minimum number of sires. For example,

./minsires is_quiet=t < Family_data_Makinen_etal.txt

gives the output

% family n L minSires<br>
 1 21  5  7<br>
 2 22  5  8<br>
 3 21  5  8<br>
 4 21  5  7<br>
 5 23  5  4<br>
 6 23  5  8<br>
 7 23  5  5<br>
 8 23  5  8
 
where family is the identity of the family, n is the number of progeny in the family, L is the number of loci (which may vary from one family to the next), and minSires is the minimum number of sires.
